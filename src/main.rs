use std::fs;
use std::thread;
use std::net::{TcpListener, TcpStream};
use std::io::{Read, Write};
use webserver_with_only_sockets::ThreadPool;


fn main() {
    let listener = TcpListener::bind("0.0.0.0:7878").expect("Listener spawn error");
    let pool = ThreadPool::new(10).unwrap();
    for stream in listener.incoming() {
        let stream = stream.expect("stream unwrap error");
        pool.execute(|| {
            handle_conn(stream);
        });
    }
}

fn handle_conn(mut stream: TcpStream) {
    let mut buf: [u8;1024] = [0; 1024];
    stream.read(&mut buf).expect("read stream error");
    let get_header = b"GET / HTTP/1.1\r\n";
    let (status, contents) = if buf.starts_with(get_header) {
        ("HTTP/1.1 200 OK\r\n\r\n", "assets/hello.html")
    } else {
        ("HTTP/1.1 404 NOT FOUND\r\n\r\n", "assets/404.html")
    };
    let contents = fs::read_to_string(contents).expect("read html error");
    let response = format!("{}{}", status, contents);
    stream.write(response.as_bytes()).unwrap();
    stream.flush().unwrap();
}


